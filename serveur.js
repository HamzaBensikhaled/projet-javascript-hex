const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const io = new require("socket.io")(server);
app.get('/', (request, response) => {
    response.sendFile('clientHex2_0.html', { root: __dirname });
});
server.listen(8888, () => { console.log('Le serveur écoute sur le port 8888'); });

var tailleDamier = -1;
var joueurs = [];
var tour = -1;
var tournum = 1;
var ListeCase = [];
var nbJoueur = 2;
var Tourde = 1;
var nbPiontSpe = 3;

// Variable pour initialise les listes des hexagones:
var e = 0;
var a = 0;
var r = 0;
var c = 0;

var HexN = [];
var HexS = [];
var HexO = [];
var HexE = [];

io.on('connection', (socket) => {

    socket.on('MiseEnPage', () => {
        if (joueurs.length > 0) {
            socket.emit('MiseEnPage2',);
        }
    });

    /* Gestion des entrées et sorties */

    socket.on('entre', (data) => {
        if (joueurs.includes(data.nom)) {
            console.log("erreur,", data.nom, "est déjà pris");
            io.emit("nomDejaPris", data.nom);
        }
        else {
            console.log("Entrée dans la partie de", data.nom);
            joueurs.push(data.nom);
            if (joueurs.length == 1) {
                tailleDamier = Number(data.taille);
                nbJoueur = Number(data.nbJoueur);
            }
            io.emit("entree", { "joueurs": joueurs, "joueur": data.nom, "taille": tailleDamier, "nbJoueur": nbJoueur });
        }
    })

    socket.on('sortie', (nom) => {
        console.log(nom);
        for (var i = 0; i < joueurs.length; i++) {
            if (joueurs[i] == nom) {
                joueurs.splice(i, 1);
                console.log('liste des joueurs apres sortie de', nom, joueurs);
                io.emit('unePersonneEstSortie', nom);
            }

        }
    })

    /* --------------------------------------------------------------------------------------------- */

    /* Voir la liste des joueurs */

    socket.on('voirJoueurs', (data) => {
        console.log("Liste des joueurs(button voir joueur):", joueurs, "demande par", data);
        socket.emit("listeJ", { "joueurs": joueurs, "numJoueur": data });
    })

    /* --------------------------------------------------------------------------------------------- */

    /* Gestion des messages */

    socket.on('envoyer', message => {
        io.emit("messagerecu", { "message": message.message, "nom": message.nom });
    })

    /* --------------------------------------------------------------------------------------------- */

    /* Gestion de la pose des pionts et des tours */

    socket.on('lancer', (data) => {
        tour = 0;
        // Initialise les listes des hexagones du Nord,Sud,Ouest,Est.
        for (var i = 0; i < (tailleDamier * tailleDamier); i++) {
            ListeCase[i] = [-1, ""];
        }
        for (var i = 0; i < tailleDamier; i++) {
            HexN[e] = i;
            e += 1;
        }
        for (var i = (tailleDamier * tailleDamier) - tailleDamier; i < (tailleDamier * tailleDamier); i++) {
            HexS[a] = i;
            a += 1;
        }
        for (var i = 0; i <= (tailleDamier * tailleDamier) - tailleDamier; i = Number(i) + Number(tailleDamier)) {
            HexO[r] = i;
            r += 1;
        }
        for (var i = tailleDamier - 1; i < (tailleDamier * tailleDamier); i = Number(i) + Number(tailleDamier)) {
            HexE[c] = i;
            c += 1;
        }
        io.emit('initialise', data)
    })

    socket.on('demandePose', data => {
        if (tour == data.numJoueur) {
            if (data.piont == 1) {
                if (ListeCase[data.NumDeCase][0] == -1) {
                    if (data.typePiont == 'Piont Normal') {
                        ListeCase[data.NumDeCase] = [data.numJoueur, data.typePiont];
                        if (Tourde == nbJoueur) { Tourde = 0; }

                        io.emit('ok', { "numJoueur": data.numJoueur, "Idcase": data.Idcase, "NumDeCase": data.NumDeCase, "typePiontOk": data.typePiont, "Tourde": Tourde, "nbPiontSpe": nbPiontSpe });
                        Tourde += 1;
                        rechercheVictoire(Number(data.NumDeCase), data.numJoueur, data.nomJoueur);
                        if (tour == nbJoueur - 1) {
                            tour = 0;
                            tournum += 1;
                            io.emit('tournum', tournum);
                        }
                        else {
                            tour += 1;
                        }

                    }
                    else {
                        if (data.nbPiontSpe > 0) {
                            ListeCase[data.NumDeCase] = [-2, data.typePiont];
                            nbPiontSpe = data.nbPiontSpe - 1;
                            console.log(nbPiontSpe, "nbpiont")
                            if (Tourde == nbJoueur) { Tourde = 0; }
                            io.emit('ok', { "numJoueur": data.numJoueur, "Idcase": data.Idcase, "NumDeCase": data.NumDeCase, "typePiontOk": data.typePiont, "Tourde": Tourde, "nbPiontSpe": nbPiontSpe });
                            Tourde += 1;
                            rechercheVictoire(Number(data.NumDeCase), data.numJoueur, data.nomJoueur);
                            if (tour == nbJoueur - 1) {
                                tour = 0;
                                tournum += 1;
                                io.emit('tournum', tournum);
                            }
                            else {
                                tour += 1;
                            }
                        }
                        else { socket.emit('non,'); }
                    }
                }
            }
            else { socket.emit('non',); }
        }
        else {
            socket.emit('pastontour',);
        }
    })

    /* --------------------------------------------------------------------------------------------- */

    /* Gestion de la victoire */



    function hexaConnexes(pos, numJoueur, ListeHexaConnexes) {

        // Partie qui se spécialise pour les dé speciaux Ouest-Est
        if (ListeCase[pos][1] == "Piont Normal" || ListeCase[pos][1] == "Ouest-Est") {
            if (ListeCase[pos][1] == "Piont Normal" & ListeHexaConnexes.includes(pos) == false) { ListeHexaConnexes.push(pos - 1 + 1); }
            if (Number(pos) - Number(1) >= 0) {
                if ((HexO.includes(pos) == false) & (ListeHexaConnexes.includes(pos - 1) == false) & (ListeCase[Number(pos) - Number(1)][Number(0)] == numJoueur || ListeCase[Number(pos) - Number(1)][Number(0)] == -2)) {
                    if (ListeCase[Number(pos) - Number(1)][Number(1)] == "Ouest-Est") {
                        ListeHexaConnexes.push(pos - 1);
                        if (ListeHexaConnexes.includes(pos) == false) { ListeHexaConnexes.push(pos); }
                    }
                    hexaConnexes(Number(pos) - 1, numJoueur, ListeHexaConnexes);

                }
            }
            if (Number(pos) + Number(1) < tailleDamier * tailleDamier) {
                if ((HexE.includes(pos) == false) & (ListeHexaConnexes.includes(pos + 1) == false) & (ListeCase[Number(pos) + Number(1)][Number(0)] == numJoueur || ListeCase[Number(pos) + Number(1)][Number(0)] == -2)) {
                    if (ListeCase[Number(pos) + Number(1)][Number(1)] == "Ouest-Est") {
                        ListeHexaConnexes.push(pos + 1);
                        if (ListeHexaConnexes.includes(pos) == false) { ListeHexaConnexes.push(pos); }
                    }
                    hexaConnexes(pos + 1, numJoueur, ListeHexaConnexes);
                }
            }
            if (ListeHexaConnexes.includes(Number(pos) - 1) & ListeHexaConnexes.includes(Number(pos) + 1) & ListeHexaConnexes.includes(Number(pos)) == false) { ListeHexaConnexes.push(pos - 1 + 1); }

        }

        // Partie qui se spécialise pour les dé speciaux NE-SO
        if (ListeCase[pos][1] == "Piont Normal" || ListeCase[pos][1] == "NE-SO") {
            if (ListeCase[pos][1] == "Piont Normal" & ListeHexaConnexes.includes(pos) == false) { ListeHexaConnexes.push(pos - 1 + 1); }
            if (Number(pos) - (Number(tailleDamier) - Number(1)) >= 0) {
                if ((HexE.includes(pos) == false) & (HexN.includes(pos) == false) & (ListeHexaConnexes.includes(Number(pos) - (tailleDamier - 1)) == false) & (ListeCase[Number(pos) - (Number(tailleDamier) - Number(1))][Number(0)] == numJoueur || ListeCase[Number(pos) - (Number(tailleDamier) - Number(1))][Number(0)] == -2)) {
                    if (ListeCase[Number(pos) - (Number(tailleDamier) - Number(1))][Number(0)] == "NO-SE") {
                        ListeHexaConnexes.push(pos - (Number(tailleDamier) - Number(1)));
                        if (ListeHexaConnexes.includes(pos) == false) { ListeHexaConnexes.push(pos); }
                    }
                    hexaConnexes(Number(pos) - (tailleDamier - 1), numJoueur, ListeHexaConnexes);
                }
            }
            if (Number(pos) + (Number(tailleDamier) - Number(1)) < tailleDamier * tailleDamier) {
                if ((HexO.includes(pos) == false) & (HexS.includes(pos) == false) & (ListeHexaConnexes.includes(Number(pos) + (tailleDamier - 1)) == false) & ((ListeCase[Number(pos) + (Number(tailleDamier) - Number(1))][Number(0)]) == numJoueur || ListeCase[Number(pos) + (Number(tailleDamier) - Number(1))][Number(0)] == -2)) {
                    if (ListeCase[Number(pos) + (Number(tailleDamier) - Number(1))][Number(0)] == "NO-SE") {
                        ListeHexaConnexes.push(pos + (Number(tailleDamier) - Number(1)));
                        if (ListeHexaConnexes.includes(pos) == false) { ListeHexaConnexes.push(pos); }
                    }
                    hexaConnexes(Number(pos) + (tailleDamier - 1), numJoueur, ListeHexaConnexes);

                }
            }
            if (ListeHexaConnexes.includes(Number(pos) - (Number(tailleDamier) - Number(1))) & ListeHexaConnexes.includes(Number(pos) + (Number(tailleDamier) - Number(1))) & ListeHexaConnexes.includes(Number(pos)) == false) { ListeHexaConnexes.push(pos - 1 + 1); }
        }

        // Partie qui se spécialise pour les dé speciaux NO-SE
        if (ListeCase[pos][1] == "Piont Normal" || ListeCase[pos][1] == "NO-SE") {
            if (ListeCase[pos][1] == "Piont Normal" & ListeHexaConnexes.includes(pos) == false) { ListeHexaConnexes.push(pos - 1 + 1); }
            if (Number(pos) - Number(tailleDamier) >= 0) {
                if ((HexN.includes(pos) == false) & (ListeHexaConnexes.includes(Number(pos) - tailleDamier) == false) & (ListeCase[Number(pos) - Number(tailleDamier)][Number(0)] == numJoueur || ListeCase[Number(pos) - Number(tailleDamier)][Number(0)] == -2)) {
                    if (ListeCase[Number(pos) - Number(tailleDamier)][Number(0)] == "NE-SO") {
                        ListeHexaConnexes.push(pos - Number(tailleDamier));
                        if (ListeHexaConnexes.includes(pos) == false) { ListeHexaConnexes.push(pos); }
                    }
                    hexaConnexes(Number(pos) - Number(tailleDamier), numJoueur, ListeHexaConnexes);

                }
            }
            if (Number(pos) + Number(tailleDamier) < tailleDamier * tailleDamier) {
                if ((HexS.includes(pos) == false) & (ListeCase[Number(pos) + tailleDamier][Number(0)] == numJoueur || ListeCase[Number(pos) + Number(tailleDamier)][Number(0)] == -2) & (ListeHexaConnexes.includes(Number(pos) + Number(tailleDamier)) == false)) {
                    if (ListeCase[Number(pos) + Number(tailleDamier)][Number(0)] == "NE-SO") {
                        ListeHexaConnexes.push(pos + Number(tailleDamier));
                        if (ListeHexaConnexes.includes(pos) == false) { ListeHexaConnexes.push(pos); }
                    }
                    hexaConnexes(Number(pos) + tailleDamier, numJoueur, ListeHexaConnexes);
                }
            }
            if (ListeHexaConnexes.includes(Number(pos) - Number(tailleDamier)) & ListeHexaConnexes.includes(Number(pos) + Number(tailleDamier)) & ListeHexaConnexes.includes(Number(pos)) == false) { ListeHexaConnexes.push(pos - 1 + 1); }

        }

        //Quand l'hex spécial Ouest-Est est dans les cases de l'ouest ou de l'est:
        if (HexO.includes(pos) & ListeCase[pos][1] == "Ouest-Est") {
            if ((ListeHexaConnexes.includes(pos + 1) == false) & (ListeCase[Number(pos) + Number(1)][Number(0)] == numJoueur || ListeCase[Number(pos) + Number(1)][Number(0)] == -2)) {
                hexaConnexes(pos + 1, numJoueur, ListeHexaConnexes);
            }
            if (ListeHexaConnexes.includes(Number(pos) + 1) & ListeHexaConnexes.includes(Number(pos)) == false) { ListeHexaConnexes.push(pos - 1 + 1); }
        }

        if (HexE.includes(pos) & ListeCase[pos][1] == "Ouest-Est") {
            if ((ListeHexaConnexes.includes(pos - 1) == false) & (ListeCase[Number(pos) - Number(1)][Number(0)] == numJoueur || ListeCase[Number(pos) - Number(1)][Number(0)] == -2)) {
                hexaConnexes(pos + 1, numJoueur, ListeHexaConnexes);
            }
            if (ListeHexaConnexes.includes(Number(pos) - 1) & ListeHexaConnexes.includes(Number(pos)) == false) { ListeHexaConnexes.push(pos - 1 + 1); }
        }

        //Quand l'hex spécial NO-SE est dans les cases du nord ou du sud:
        if (HexN.includes(pos) & ListeCase[pos][1] == "NO-SE") {
            if ((ListeHexaConnexes.includes(pos + tailleDamier) == false) & (ListeCase[Number(pos) + tailleDamier][Number(0)] == numJoueur || ListeCase[Number(pos) + tailleDamier][Number(0)] == -2)) {
                hexaConnexes(pos + tailleDamier, numJoueur, ListeHexaConnexes);
            }
            if (ListeHexaConnexes.includes(Number(pos) + tailleDamier) & ListeHexaConnexes.includes(Number(pos)) == false) { ListeHexaConnexes.push(pos - 1 + 1); }
        }

        if (HexS.includes(pos) & ListeCase[pos][1] == "NO-SE") {
            if ((ListeHexaConnexes.includes(pos - tailleDamier) == false) & (ListeCase[Number(pos) - tailleDamier][Number(0)] == numJoueur || ListeCase[Number(pos) - tailleDamier][Number(0)] == -2)) {
                hexaConnexes(pos + tailleDamier, numJoueur, ListeHexaConnexes);
            }
            if (ListeHexaConnexes.includes(Number(pos) - tailleDamier) & ListeHexaConnexes.includes(Number(pos)) == false) { ListeHexaConnexes.push(pos - 1 + 1); }
        }

        //Quand l'hex spécial NE-SO est dans les cases du nord ou du sud:
        if (HexN.includes(pos) & ListeCase[pos][1] == "NE-SO") {
            if ((ListeHexaConnexes.includes(pos + tailleDamier - 1) == false) & (ListeCase[Number(pos) + tailleDamier - 1][Number(0)] == numJoueur || ListeCase[Number(pos) + tailleDamier - 1][Number(0)] == -2)) {
                hexaConnexes(pos + tailleDamier - 1, numJoueur, ListeHexaConnexes);
            }
            if (ListeHexaConnexes.includes(Number(pos) + tailleDamier - 1) & ListeHexaConnexes.includes(Number(pos)) == false) { ListeHexaConnexes.push(pos - 1 + 1); }
        }

        if (HexS.includes(pos) & ListeCase[pos][1] == "NE-SO") {
            if ((ListeHexaConnexes.includes(pos - tailleDamier - 1) == false) & (ListeCase[Number(pos) - tailleDamier - 1][Number(0)] == numJoueur || ListeCase[Number(pos) - tailleDamier - 1][Number(0)] == -2)) {
                hexaConnexes(pos - tailleDamier - 1, numJoueur, ListeHexaConnexes);
            }
            if (ListeHexaConnexes.includes(Number(pos) - tailleDamier - 1) & ListeHexaConnexes.includes(Number(pos)) == false) { ListeHexaConnexes.push(pos - 1 + 1); }
        }

        console.log(ListeHexaConnexes);
        return ListeHexaConnexes;

    }
    function rechercheVictoire(pose, numJ, nom) {
        var bordN = false;
        var bordS = false;
        var bordO = false;
        var bordE = false;

        var listeConnexes = [];
        hexaConnexes(pose, numJ, listeConnexes);

        if (numJ == 0 || numJ == 2) {
            for (var i = 0; i < HexN.length; i++) {
                if (listeConnexes.includes(HexN[i]) == true) {
                    bordN = true;
                }
            }
            for (var i = 0; i < HexS.length; i++) {
                if (listeConnexes.includes(HexS[i]) == true) {
                    bordS = true;
                }
            }
            if (bordN == true & bordS == true) {
                io.emit("victoire", nom);
            }
        }

        if (numJ == 1 || numJ == 3) {
            for (var i = 0; i < HexO.length; i++) {
                if (listeConnexes.includes(HexO[i]) == true) {
                    bordO = true;
                }
            }
            for (var i = 0; i < HexE.length; i++) {
                if (listeConnexes.includes(HexE[i]) == true) {
                    bordE = true;
                }
            }

            if (bordO == true & bordE == true) {
                io.emit("victoire", nom);
            }

        }
    }

    /* --------------------------------------------------------------------------------------------- */

    socket.on('rejouer', () => {
        ListeCase = [];
        e = 0;
        a = 0;
        r = 0;
        c = 0;

        HexN = [];
        HexS = [];
        HexO = [];
        HexE = [];
        tailleDamier = -1;
        joueurs = [];
        tour = -1;
        tournum = 1;
        ListeCase = [];
        nbJoueur = 2;
        Tourde = 1;
        io.emit('relance',);
    })
});
