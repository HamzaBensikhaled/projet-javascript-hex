#Projet JavaScript Hex

##Hamza Bensikhaled | ##Mustapha Lamine Maatou | ##Rayan Hasnaoui-Mounir

- Fonction d'entrée et de sortie.
- Choix de la taille du tablier et du nombre de joueur par le premier joueur.
- Lancement de la partie par le premier joueur (bouton lancer se voit que depuis la page du premier joueur).
- Voir les joueurs, tchat.
- Héxagones spéciaux (3 max par personne).
- Victoire (Héxagones spéciaux compris) + bouton rejouer une partie.

